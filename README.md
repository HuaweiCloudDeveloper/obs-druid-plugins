### 背景

Druid专为需要快速数据查询与摄入的工作流程而设计，在即时数据可见性、即席查询、运营分析以及高并发等方面表现非常出色。

### 项目简介

Druid的深度存储目前支持S3，HDFS，目前已有方案[HuaweiCloud OBS Druid](https://support.huaweicloud.com/bestpractice-obs/obs_05_1514.html)通过HDFS接口对接OBS，使用OBS提供的OBSA-HDFS工具，无需重新编译druid，将OBS配置为deep storage，此方案依赖Hadoop，仅作为参考，目的是扩展Druid，使其像支持S3一样支持OBS，不需要部署额外的组件，将代码贡献到Druid，提供功能文档，使用文档，示例代码等等...

### 对接步骤

#### 1.配置Druid
 a. 修改配置：

    conf/druid/single-server/micro-quickstart/_common/common.runtime.properties
  将druid-hdfs-storage加入druid.extensions.loadList。![img](https://support.huaweicloud.com/bestpractice-obs/zh-cn_image_0000001180642975.png)

b. 配置Deep storage在OBS中的存储路径。![img](https://support.huaweicloud.com/bestpractice-obs/zh-cn_image_0000001134640260.png)![img](https://support.huaweicloud.com/bestpractice-obs/zh-cn_image_0000001134640260.png)

#### 2.配置OBSA-HDFS插件

a.执行以下命令，下载OBSA-HDFS插件，拷贝到extensions/druid-hdfs-storage/ 目录。

以hadoop-huaweicloud-2.8.3.36.jar为例。

wget https://github.com/huaweicloud/obsa-hdfs/blob/master/release/hadoop-huaweicloud-2.8.3-hw-42.jar

b.在配置目录conf/druid/single-server/micro-quickstart/_common/下增加hdfs-site.xml，配置如下：

```
<configuration>
	<property>
		<name>fs.obs.access.key</name>
		<value>*********</value>
	</property>
	<property>
		<name>fs.obs.secret.key</name>
		<value>*********</value>
	</property>
	<property>
		<name>fs.obs.endpoint</name>
		<value>obs.cn-north-4.myhuaweicloud.com</value>
	</property>
	<property>
		<name>fs.obs.buffer.dir</name>
		<value>/home/modules/data/buf</value>
	</property>
	<property>
		<name>fs.obs.impl</name>
		<value>org.apache.hadoop.fs.obs.OBSFileSystem</value>
	</property>
</configuration>
```

注:hadoop-huaweicloud-2.8.3.36.jar大小在几MB，如果下载的有问题，可以在[这里](https://github.com/huaweicloud/obsa-hdfs/tree/master/release)进行下载

hdfs-site.xml中配置fs.obs.endpoint，一定要选择自己OBS所在区

### 参考资料

- [OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1507.html)
- [Druid](https://github.com/apache/druid)